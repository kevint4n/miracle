import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CToggler,
  CLink,
  CNavbar,
  CNavbarBrand,
  CNavbarNav,
  CButton,
  CDropdown,
  CDropdownToggle,
  CDropdownItem,
  CDropdownMenu,
  CModal,
  CModalBody,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CCol,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { cilCart, cilUser } from "@coreui/icons";
import swal from "sweetalert2";
import { useHistory } from "react-router-dom";

const TheHeader = () => {
  const [modal, setModal] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const sidebarShow = useSelector((state) => state.sidebarShow);
  const signedIn = useSelector((state) => state.currentUser);
  const [email, setEmail] = useState("miranda_b@gmail.com");

  const toggleSidebar = () => {
    const val = [true, "responsive"].includes(sidebarShow)
      ? false
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  const handleSignIn = () => {
    setModal(false);
    dispatch({ type: "setCurretUser", userEmail: email });
    swal
      .mixin({
        toast: true,
        position: "top",
        showConfirmButton: false,
        timer: 2000,
      })
      .fire({
        icon: "success",
        title: "Sucessfully Signed In",
      });
  };

  const handleSignOut = () => {
    dispatch({ type: "setCurretUser", userEmail: "logout" });
    swal
      .mixin({
        toast: true,
        position: "top",
        showConfirmButton: false,
        timer: 2000,
      })
      .fire({
        icon: "success",
        title: "Sucessfully Signed Out",
      });
  };

  const handleUserName = (e) => {
    setEmail(e.target.value);
  };

  return (
    <div>

      <CNavbar color="secondary" style={{ padding: "0px" }}>
        <CToggler
          inHeader
          className="ml-3 d-md-down-none"
          onClick={toggleSidebar}
        />

        <CNavbarBrand to="/">
          <img
            alt=""
            onClick={() => history.push(`/`)}
            style={{
              width: "70px",
              height: "50px",
              cursor: "pointer",
              marginRight: "10px",
            }}
            src="logo/miracle.jpeg"
          />
        </CNavbarBrand>
        <CNavbarNav
          className="ml-auto"
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          {signedIn ? (
            <span
              style={{ marginRight: "5px" }}
            >{`Welcome, ${signedIn.name}`}</span>
          ) : (
            ""
          )}
          <CButton
            color="primary"
            className="my-2 my-sm-0 mr-2"
            to="/cart"
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <CIcon content={cilCart} />
          </CButton>

          {signedIn ? (
            <CDropdown className="m-1 btn-group">
              <CDropdownToggle color="primary">
                <CIcon content={cilUser} />
              </CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem onClick={handleSignOut}>Sign Out</CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          ) : (
            <CButton
              color="primary"
              className="my-2 my-sm-0 mr-2"
              onClick={() => setModal(!modal)}
            >
              Sign In
            </CButton>
          )}
        </CNavbarNav>
      </CNavbar>

      <CModal show={modal} onClose={() => setModal(!modal)}>
        <CModalBody>
          <CForm>
            <h1>Sign In</h1>
            <p className="text-muted">Sign In to your account</p>
            <CInputGroup className="mb-3">
              <CInputGroupPrepend>
                <CInputGroupText>
                  <CIcon name="cil-user" />
                </CInputGroupText>
              </CInputGroupPrepend>
              <CInput type="text" onChange={handleUserName} value={email} />
            </CInputGroup>
            <CInputGroup className="mb-4">
              <CInputGroupPrepend>
                <CInputGroupText>
                  <CIcon name="cil-lock-locked" />
                </CInputGroupText>
              </CInputGroupPrepend>
              <CInput type="password" value={"password"} />
            </CInputGroup>
            <CRow>
              <CCol xs="6">
                <CButton
                  color="primary"
                  className="px-4"
                  onClick={handleSignIn}
                >
                  Sign In
                </CButton>
              </CCol>
              <CCol xs="6" className="text-right">
                <CButton color="link" className="px-0" to="/register">
                  Register here
                </CButton>
              </CCol>
            </CRow>
          </CForm>
        </CModalBody>
      </CModal>
    </div>
  );
};

export default TheHeader;
